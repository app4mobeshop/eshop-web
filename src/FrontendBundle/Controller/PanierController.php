<?php

namespace FrontendBundle\Controller;

use FrontendBundle\Form\UtilisateursAdressesType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use WebBundle\Entity\UtilisateursAdresses;

class PanierController extends BaseController
{
    public function menuAction()
    {
        $this->initSession();
        $this->initPanier();
        $this->initEntityManager();

        $articles = $this->getProduitsByIds(array_keys($this->panier));


        return $this->render('FrontendBundle:Blocks:navbar.html.twig', array('panier' => $this->panier, 'articles' => $articles));
    }

    /**
     * @Route(path="/delete_item_from_cart/{id}",options={"expose"=true}, name="delete_item_from_cart")
     */
    public function supprimerAction($id)
    {

        $this->initSession();
        $this->initPanier();

        if (array_key_exists($id, $this->panier)) {
            unset($this->panier[$id]);
            $this->session->set('panier', $this->panier);
            //$this->get('session')->getFlashBag()->add('success', 'Article supprimé avec succès');
        }

        return $this->menuAction();
    }

    /**
     * @Route(path="/add_item_to_cart/{id}",options={"expose"=true},  name="add_item_to_cart")
     */
    public function ajouterAction($id)
    {
        $this->initSession();
        $this->initPanier();

        $panier = $this->panier;

        if (array_key_exists($id, $panier)) {
            $panier[$id]++;
            //$this->get('session')->getFlashBag()->add('success','Quantité modifié avec succès');
        } else {
            if (is_null($this->getRequest()->query->get('qte')))
                $panier[$id] = 1;
            else
                $panier[$id] = $this->getRequest()->query->get('qte');
            // $this->get('session')->getFlashBag()->add('success', 'Article ajouté avec succès');
        }

        $this->session->set('panier', $panier);

        return $this->menuAction();
    }

    /**
     * @Route(path="/update_cart",options={"expose"=true},  name="update_cart")
     */
    public function updateAction(Request $request)
    {
        $this->initSession();
        $panier = $this->initPanier();

        $qts = $request->query->get('qte');


        $i = 0;
        foreach ($panier as $key => $value) {
            $panier[$key] = $qts[$i];
            $i++;
        }

        $this->session->set('panier', $panier);

        return $this->redirect($this->generateUrl('panier'));
    }


    /**
     * @Route(path="/panier",  name="panier")
     */
    public function panierAction()
    {
        $this->initSession();
        $panier = $this->initPanier();
        $date = $this->initDate();
        $this->initEntityManager();


        $produits = $this->getProduitsByIds(array_keys($this->panier));

        return $this->render('FrontendBundle:Panier:panier.html.twig',
            array(
                'produits' => $produits,
                'panier' => $panier,
                "date" => $date
            ));
    }

    /**
     * @Route(path="/deleteAdresse/{id}",  name="deleteAdresse")
     */
    public function adresseSuppressionAction(Request $request, $id)
    {
        $em = $this->initEntityManager();
        $entity = $em->getRepository('WebBundle:UtilisateursAdresses')->find($id);

        if ($this->getUser() != $entity->getUtilisateur() || !$entity)
            return $this->redirect($this->generateUrl('livraison'));

        $em->remove($entity);
        $em->flush();

        if($request->get("from_profile",0) == 1)
            return $this->redirect($this->generateUrl('profile_adresse'));

        return $this->redirect($this->generateUrl('livraison'));
    }


    /**
     * @Route(path="/livraison",  name="livraison")
     */
    public function livraisonAction(Request $request)
    {
        $this->initSession();
        $panier = $this->initPanier();
        $date = $this->initDate();
        $em = $this->initEntityManager();
        $utilisateur = $this->getUser();
        $entity = new UtilisateursAdresses();
        $form = $this->createForm(new UtilisateursAdressesType($em), $entity);


        if ($this->get('request')->getMethod() == 'POST') {
            $form->handleRequest($this->getRequest());
            //if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity->setUtilisateur($utilisateur);

                $delegation = $em->getRepository('WebBundle:Delegation')->findOneBy(array('code' => $entity->getCp()));
                if ($delegation) {
                    $entity->setPays($delegation->getRegion()->getName());
                    $entity->setVille($delegation->getName());
                }

                $em->persist($entity);
                $em->flush();

                return $this->redirect($this->generateUrl('livraison'));
            //}
        }

        return $this->render('FrontendBundle:Panier:livraison.html.twig', array('utilisateur' => $utilisateur,
            'form' => $form->createView(),'panier'=>$panier));
    }

    public function setLivraisonOnSession($livraison,$facturation)
    {
        $session = $this->getRequest()->getSession();
        //var_dump($livraison);die;
        if (!$session->has('adresse')) $session->set('adresse', array());
        $adresse = $session->get('adresse');


        if ($livraison != null && $facturation != null) {
            $adresse['livraison'] = $livraison;
            $adresse['facturation'] = $facturation;


        } else {
            return $this->redirect($this->generateUrl('validation'));
        }

        $session->set('adresse', $adresse);
        return $this->redirect($this->generateUrl('validationAdresse'));
    }


    /**
     * @Route(path="/validationAdresse",  name="validationAdresse")
     */
    public function validationAction(Request $request)
    {
        $periode = $request->get("periode", 1);
        $livraison = $request->get("livraison");
        $facturation = $request->get("facturation");
        $date = $request->get("date", date("Y-m-d"));
        $session = $this->initSession();
        $session_date = $this->initDate();

        if ($request->query->has("periode") && $request->query->has("date")) {
            $session_date["periode"] = $periode;
            $session_date["date"] = $date;
            $this->session->set("date", $session_date);
        }

        if ($this->get('request')->getMethod() == 'GET')
            $this->setLivraisonOnSession($livraison,$facturation);
        //var_dump($this->session->get("adresse"));die("ddd");

        $em = $this->getDoctrine()->getManager();

        $prepareCommande = $this->forward('FrontendBundle:Commandes:prepareCommande');
        //var_dump($prepareCommande->getContent()); die();
        $commande = $em->getRepository('WebBundle:Commandes')->find($prepareCommande->getContent());
        
        //var_dump($commande); die();

        return $this->render('FrontendBundle:Panier:validation.html.twig', array('commande' => $commande));
    }


    /**
     * @Route(path="/delegations/{cp}",options={"expose"=true},  name="delegations")
     */
    public function delegationsAction(Request $request, $cp)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $villeCodePostal = $em->getRepository('WebBundle:Delegation')->findBy(array('code' => $cp));

            if ($villeCodePostal) {
                $villes = array();
                foreach ($villeCodePostal as $ville) {
                    $villes[str_replace("  ", "", $ville->getName())] = $ville->getName();
                }
            } else {
                $villes = null;
            }

            $response = new JsonResponse();
        //var_dump($response->setData(array('ville' => $villes)));die();
            return $response->setData(array('ville' => $villes));
        } else {
            throw new \Exception('Erreur');
        }
    }
}
