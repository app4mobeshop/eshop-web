<?php

namespace VetrineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/",name="howto_create_shop")
     */
    public function indexAction()
    {
        return $this->render('VetrineBundle:Default:index.html.twig');
    }
    /**
     * @Route("/register",name="create_shop")
     */
    public function registerAction()
    {
        return $this->render('VetrineBundle:Default:register.html.twig');
    }
}
