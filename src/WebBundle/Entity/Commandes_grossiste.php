<?php

namespace WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Commandes_grossiste
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $client_id;


    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $grossiste_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $commande_id;



    /**
     * @ORM\Column(type="string")
     */
    private $status;


    /**
     * @ORM\Column(type="datetime")
     */
    private $delivery_date;

    /**
     * @ORM\OneToMany(targetEntity="DetailCommand", mappedBy="command")
     */
    private $products;

    /**
     * @ORM\ManyToOne(targetEntity="Commandes")
     * @ORM\JoinColumn(name="commande_id", referencedColumnName="id")
     */
    private $commande;


    /**
     * @ORM\ManyToOne(targetEntity="Utilisateurs")
     * @ORM\JoinColumn(name="grossiste_id", referencedColumnName="id")
     */
    private $grossiste;

    /**
     * @ORM\ManyToOne(targetEntity="Utilisateurs")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client_id
     *
     * @param integer $clientId
     * @return Command
     */
    public function setClientId($clientId)
    {
        $this->client_id = $clientId;

        return $this;
    }

    /**
     * Get client_id
     *
     * @return integer 
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * Set grossiste_id
     *
     * @param integer $grossisteId
     * @return Command
     */
    public function setGrossisteId($grossisteId)
    {
        $this->grossiste_id = $grossisteId;

        return $this;
    }

    /**
     * Get grossiste_id
     *
     * @return integer 
     */
    public function getGrossisteId()
    {
        return $this->grossiste_id;
    }

    /**
     * Set total_price
     *
     * @param float $totalPrice
     * @return Command
     */
    public function setTotalPrice($totalPrice)
    {
        $this->total_price = $totalPrice;

        return $this;
    }

    /**
     * Get total_price
     *
     * @return float 
     */
    public function getTotalPrice()
    {
        return $this->total_price;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Command
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set delivery_address
     *
     * @param float $deliveryAddress
     * @return Command
     */
    public function setDeliveryAddress($deliveryAddress)
    {
        $this->delivery_address = $deliveryAddress;

        return $this;
    }

    /**
     * Get delivery_address
     *
     * @return float 
     */
    public function getDeliveryAddress()
    {
        return $this->delivery_address;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Command
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set last_update
     *
     * @param \DateTime $lastUpdate
     * @return Command
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get last_update
     *
     * @return \DateTime 
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Add products
     *
     * @param \WebBundle\Entity\DetailCommand $products
     * @return Command
     */
    public function addProduct(\WebBundle\Entity\DetailCommand $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \WebBundle\Entity\DetailCommand $products
     */
    public function removeProduct(\WebBundle\Entity\DetailCommand $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set grossiste
     *
     * @param \WebBundle\Entity\Utilisateurs $grossiste
     * @return Command
     */
    public function setGrossiste(\WebBundle\Entity\Utilisateurs $grossiste = null)
    {
        $this->grossiste = $grossiste;

        return $this;
    }

    /**
     * Get grossiste
     *
     * @return \WebBundle\Entity\Utilisateurs 
     */
    public function getGrossiste()
    {
        return $this->grossiste;
    }

    /**
     * Set cleint
     *
     * @param \WebBundle\Entity\Utilisateurs $cleint
     * @return Command
     */
    public function setCleint(\WebBundle\Entity\Utilisateurs $cleint = null)
    {
        $this->cleint = $cleint;

        return $this;
    }

    /**
     * Get cleint
     *
     * @return \WebBundle\Entity\Utilisateurs 
     */
    public function getCleint()
    {
        return $this->cleint;
    }

    /**
     * Set commandeId
     *
     * @param integer $commandeId
     *
     * @return Commandes_grossiste
     */
    public function setCommandeId($commandeId)
    {
        $this->commande_id = $commandeId;

        return $this;
    }

    /**
     * Get commandeId
     *
     * @return integer
     */
    public function getCommandeId()
    {
        return $this->commande_id;
    }

    /**
     * Set deliveryDate
     *
     * @param \DateTime $deliveryDate
     *
     * @return Commandes_grossiste
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->delivery_date = $deliveryDate;

        return $this;
    }

    /**
     * Get deliveryDate
     *
     * @return \DateTime
     */
    public function getDeliveryDate()
    {
        return $this->delivery_date;
    }

    /**
     * Set commande
     *
     * @param \WebBundle\Entity\Commandes $commande
     *
     * @return Commandes_grossiste
     */
    public function setCommande(\WebBundle\Entity\Commandes $commande = null)
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * Get commande
     *
     * @return \WebBundle\Entity\Commandes
     */
    public function getCommande()
    {
        return $this->commande;
    }

    /**
     * Set client
     *
     * @param \WebBundle\Entity\Utilisateurs $client
     *
     * @return Commandes_grossiste
     */
    public function setClient(\WebBundle\Entity\Utilisateurs $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \WebBundle\Entity\Utilisateurs
     */
    public function getClient()
    {
        return $this->client;
    }
}
