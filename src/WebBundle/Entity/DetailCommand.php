<?php

namespace WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class DetailCommand
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $command_id;


    /**
     * @ORM\Column(type="integer")
     */
    private $product_id;


    /**
     * @ORM\Column(type="integer")
     */
    private $qt;


    /**
     * @ORM\Column(type="float")
     */
    private $price_unit;




    /**
     * @ORM\ManyToOne(targetEntity="Produits", inversedBy="commands")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;


    /**
     * @ORM\ManyToOne(targetEntity="Commandes_grossiste", inversedBy="products")
     * @ORM\JoinColumn(name="command_id", referencedColumnName="id")
     */
    private $command;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set command_id
     *
     * @param integer $commandId
     * @return DetailCommand
     */
    public function setCommandId($commandId)
    {
        $this->command_id = $commandId;

        return $this;
    }

    /**
     * Get command_id
     *
     * @return integer 
     */
    public function getCommandId()
    {
        return $this->command_id;
    }

    /**
     * Set product_id
     *
     * @param integer $productId
     * @return DetailCommand
     */
    public function setProductId($productId)
    {
        $this->product_id = $productId;

        return $this;
    }

    /**
     * Get product_id
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * Set qt
     *
     * @param integer $qt
     * @return DetailCommand
     */
    public function setQt($qt)
    {
        $this->qt = $qt;

        return $this;
    }

    /**
     * Get qt
     *
     * @return integer 
     */
    public function getQt()
    {
        return $this->qt;
    }

    /**
     * Set price_unit
     *
     * @param float $priceUnit
     * @return DetailCommand
     */
    public function setPriceUnit($priceUnit)
    {
        $this->price_unit = $priceUnit;

        return $this;
    }

    /**
     * Get price_unit
     *
     * @return float 
     */
    public function getPriceUnit()
    {
        return $this->price_unit;
    }

    /**
     * Set product
     *
     * @param \WebBundle\Entity\Produits $product
     * @return DetailCommand
     */
    public function setProduct(\WebBundle\Entity\Produits $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \WebBundle\Entity\Produits 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set command
     *
     * @param \WebBundle\Entity\Commandes_grossiste $command
     * @return DetailCommand
     */
    public function setCommand(\WebBundle\Entity\Commandes_grossiste $command = null)
    {
        $this->command = $command;

        return $this;
    }

    /**
     * Get command
     *
     * @return \WebBundle\Entity\Commandes_grossiste
     */
    public function getCommand()
    {
        return $this->command;
    }
}
